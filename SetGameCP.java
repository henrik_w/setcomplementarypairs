import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class SetGameCP {
    private static Random random = new Random(); // needs to be initialized for the tests
    private static long totalRounds = 0;
    private static long gamesWithOnlyNominalCards = 0;
    private static long[][] setCounter = new long[82][82]; // deck-size, table-size
    private static long[][] noSetCounter = new long[82][82]; // deck-size, table-size
    private static long[][] availableSets = new long[82][82]; // deck-size, table-size

    class Card implements Comparable {
        private int number;
        private int symbol;
        private int shading;
        private int colour;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Card card = (Card) o;
            if (colour != card.colour) return false;
            if (number != card.number) return false;
            if (shading != card.shading) return false;
            if (symbol != card.symbol) return false;
            return true;
        }

        @Override
        public int hashCode() {
            int result = number;
            result = 31 * result + symbol;
            result = 31 * result + shading;
            result = 31 * result + colour;
            return result;
        }

        private float sortOrder;

        public Card(int nu, int sy, int sh, int co) {
            number = nu;
            symbol = sy;
            shading = sh;
            colour = co;
            sortOrder = random.nextFloat(); // used when shuffling the deck
        }
        public int compareTo(Object other) {
            if (this.sortOrder == ((Card) other).sortOrder)
                return 0;
            else if ((this.sortOrder) > ((Card) other).sortOrder)
                return 1;
            else
                return -1;
        }

        public String toString() {
            return number + ":" + symbol + ":" + shading + ":" + colour; // + " - " + sortOrder;
        }
    }

    class Pair {
        private final Card first;
        private final Card second;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            if (second != null ? !second.equals(pair.second) : pair.second != null) return false;
            return true;
        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        Pair(Card f, Card s) {
            first = f;
            second = s;
        }

        public String toString() {
            return "{" + first + ", " + second + "}";
        }
    }

    private ArrayList<Card> createShuffledDeck() {
        ArrayList<Card> cards = new ArrayList<Card>(81);
        for (int number=0; number<3; number++) {
            for (int symbol=0; symbol<3; symbol++) {
                for (int shading=0; shading<3; shading++) {
                    for (int colour=0; colour<3; colour++) {
                        cards.add(new Card(number, symbol, shading, colour));
                    }
                }
            }
        }
        Collections.sort(cards);
        return cards;
    }

    int getSetFormingProperty(int a, int b) {
        if (a == b) {
            return a;  // all three properties are the same
        } else {
            return (3 - a - b);
        }
    }

    Card getSetFormingCard(Card a, Card b) {
        int number = getSetFormingProperty(a.number, b.number);
        int symbol = getSetFormingProperty(a.symbol, b.symbol);
        int shading = getSetFormingProperty(a.shading, b.shading);
        int colour = getSetFormingProperty(a.colour, b.colour);
        return new Card(number,  symbol, shading, colour);
    }

    ArrayList<ArrayList<Pair>> getAllComplementarySets(HashMap<Card, ArrayList<Pair>> allPairs) {
        ArrayList<ArrayList<Pair>> complementarySets = new ArrayList<ArrayList<Pair>>();
        for (Card setFormingCard: allPairs.keySet()) {
            ArrayList<Pair> pairs = allPairs.get(setFormingCard);
            if (pairs.size() > 1) {
                complementarySets.add(pairs);
            }
        }
        return complementarySets;
    }

    // For each pair, find the third card (need not be on the table) that is needed to form a set, and add the
    // pair to a list keyed with the third card. Later we can check the list and see if it contains at least
    // two pairs - if so, two such pairs are complementary pairs (CP)
    HashMap<Card, ArrayList<Pair>>  getAllPairs(List<Card> cards) {
        HashMap<Card, ArrayList<Pair>> result = new HashMap<Card, ArrayList<Pair>>(81);
        if (cards == null) return result;
        int size = cards.size();
        for (int ai = 0; ai < size; ai++) {
            Card a = cards.get(ai);
            for (int bi = ai + 1; bi < size; bi++) {
                Card b = cards.get(bi);
                Card setFormingCard = getSetFormingCard(a, b);
                Pair pair = new Pair(a, b);
                ArrayList<Pair> pairs = result.get(setFormingCard);
                if (pairs == null) {
                    pairs = new ArrayList<Pair>();
                    result.put(setFormingCard, pairs);
                }
                pairs.add(pair);
            }
        }
        return result;
    }

    // Each list of pairs has at least 2 pairs in it
    ArrayList<Card> getSet(ArrayList<ArrayList<Pair>> pairs) {
        ArrayList<Card> result = new ArrayList<Card>();
        if (pairs.size() == 0) return result;
        // pick randomly
        // first pick list
        int randIndexList = random.nextInt(pairs.size());
        ArrayList<Pair> pairList= pairs.get(randIndexList);
        // next pick start index in that list
        int randIndexPairStart = random.nextInt(pairList.size() - 1); // picking the start index
        Pair firstPair = pairList.get(randIndexPairStart);
        result.add(firstPair.first);
        result.add(firstPair.second);
        Pair secondPair = pairList.get(randIndexPairStart + 1);
        result.add(secondPair.first);
        result.add(secondPair.second);
        return result;
    }

    boolean setExists(List<Card> cards) {
        return getAllComplementarySets(getAllPairs(cards)).size() > 0;
    }

    void moveCards(ArrayList<Card> from, ArrayList<Card> to, int numberOfCards) {
        for (int i=0; i < numberOfCards; i++) {
            if (from.isEmpty()) break;
            to.add(from.remove(from.size() - 1));
        }
    }

    void removeCards(ArrayList<Card> toBeRemoved, ArrayList<Card> from) {
        for (Card card : toBeRemoved) {
            from.remove(card);
        }
    }

    void printCards(String text, ArrayList<Card> cards, int columns) {
        System.out.println(text);
        int columnCounter = 0;
        for (Card card : cards) {
            System.out.print(card + " ");
            columnCounter++;
            if (columnCounter >= columns) {
                System.out.println();
                columnCounter = 0;
            }
        }
        System.out.println();
    }

    private void play(ArrayList<Card> deck, boolean debug, int nominalTableSize) {
        int round = 0;
        boolean noSetEncountered = false;
        ArrayList<Card> table = new ArrayList<Card>();
        moveCards(deck, table, nominalTableSize);
        while (!deck.isEmpty() || setExists(table)) {
            round++;
            totalRounds++;
            HashMap<Card, ArrayList<Pair>> allPairs = getAllPairs(table);
            ArrayList<ArrayList<Pair>> complementarySets = getAllComplementarySets(allPairs);
            availableSets[deck.size()][table.size()] += complementarySets.size();
            ArrayList<Card> set = getSet(complementarySets);
            boolean setFound = set.size() > 0;
            if (debug) {
                System.out.println("Round " + round + " deck size=" + deck.size() + " table size=" + table.size());
                printCards("Table", table, 3);
                System.out.println("Found pairs: ");
                for (Card setFormingCard: allPairs.keySet()) {
                    System.out.println(setFormingCard + " - " + allPairs.get(setFormingCard));
                }
                System.out.println("Found " + complementarySets.size() + " Pair-lists:");
                int setCounter = 0;
                for (ArrayList<Pair> pairs  : complementarySets) {
                    System.out.println("Pair-list " + ++setCounter + " : " + pairs);
                }
                printCards("Removing these", set, 4);
                if (complementarySets.size() == 0) System.out.println("No Complementary Pairs found!!!\n");
            }
            if (setFound) {
                setCounter[deck.size()][table.size()]++;
                removeCards(set, table);
            } else {
                noSetEncountered = true;
                noSetCounter[deck.size()][table.size()]++;
            }
            if ((setFound && table.size() < nominalTableSize)) {
                moveCards(deck, table, nominalTableSize - table.size());
            } else if (!setFound) {
                moveCards(deck, table, 3);
            }
        }
        // The deck is now empty, and no more sets are on the table. Update appropriate counter for no set
        noSetCounter[deck.size()][table.size()]++;
        if (!noSetEncountered) gamesWithOnlyNominalCards++;
    }

    private static void printResults(int tableSize) {
        System.out.println("In deck |   Set  | NoSet | Set:NoSet for " + tableSize + " | Avg # of Sets");
        System.out.println("--------+--------+-------+------------------+-------------");
        for (int i = 72; i >= 0; i-= 1) {
            long set = setCounter[i][tableSize];
            long noSet = noSetCounter[i][tableSize];
            String ratioString = "        oo";
            if (noSet > 0) {
                float ratio = (float)set / noSet;
                ratioString = String.format("%10.1f", ratio);
            }
            String avgNumSet = "  -";
            long sum = set + noSet;
            if (sum > 0) {
                avgNumSet = String.format("%3.2f", (float)availableSets[i][tableSize] / (set + noSet));
            }
            if (!(set == 0 && noSet == 0)) {
                System.out.printf("   %4d |%7d |%6d |%s:1      | %s\n", i, set, noSet, ratioString, avgNumSet);
            }
        }
        System.out.println();
    }

    private static void printHelp() {
        String  helpText = "\nSetGame simulates playing the game SET, and prints statistics from the simulation.\n" +
                "In particular it prints how many times no Set is present among the cards on the table.\n\n" +
                "Possible parameters:\n" +
                "-n x      x indicates the number of games to play (default is 10000)\n" +
                "-seed x   x is the random number generator seed value. If none is given, a value is\n" +
                "          chosen and printed in the results. That way, the exact same simulation can\n" +
                "          be run again by giving the previous (printed) seed value as input argument\n" +
                "-debug    for each round in the game, prints the cards on the table, and the Set found\n" +
                "-h        help, prints this message and then stops the execution\n";
        System.out.println(helpText);
    }

    public static String asPercent(float value) {
        return String.format("%.1f", value * 100);
    }

    public static void main(String[] args) {
        int nominalTableSize = 9;
        long runs = 100000;
        long seed = (System.nanoTime() % 1000000000);
        boolean debug = false;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-n")) {
                runs = Long.parseLong(args[++i]);
            } else if (args[i].equals("-seed")) {
                seed = Long.parseLong(args[++i]);
            } else if (args[i].equals("-debug")) {
                debug = true;
            } else if (args[i].equals("-h")) {
                printHelp();
                return;
            }
        }
        System.out.println("");
        random  = new Random(seed);
        long startTime = System.currentTimeMillis();
        for (long j = 0; j < runs; j++) {
            SetGameCP game = new SetGameCP();
            ArrayList<Card> deck = game.createShuffledDeck();
            game.play(deck, debug, nominalTableSize);
        }
        long duration = (System.currentTimeMillis() - startTime) / 1000;
        System.out.println("Total games played=" + runs +  ", total rounds=" + totalRounds + ", took " + duration + " seconds.");
        System.out.println("Nominal table size=" + nominalTableSize);
        System.out.println("Games that never needed additional cards on the table: " + gamesWithOnlyNominalCards + " (" +
                asPercent((float) gamesWithOnlyNominalCards / runs) + " percent of all games)");
        System.out.println("Games where no cards remain on the table at the end: " + noSetCounter[0][0] + " (" +
                asPercent((float) noSetCounter[0][0] / runs) + " percent of all games)");
        System.out.println("Seed=" + seed + "\n");
        for (int i=0; i<=18; i++) {
            printResults(i);
        }
    }
}
